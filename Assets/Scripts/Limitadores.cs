﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limitadores : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Sonar")
        {
            coll.gameObject.GetComponent<Collider2D>().isTrigger = true;
            coll.gameObject.SetActive(false);
        }
    }
}
