﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLock : MonoBehaviour 
{
	public static bool IsActive { get; set; }

	void Awake()
	{
		#if DEVELOPMENT_BUILD

		IsActive = true;
		if (IsActive)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;	
		}

		#endif
	}
}
