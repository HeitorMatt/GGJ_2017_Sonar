﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAlarm : MonoBehaviour {
	public GameObject pivotSonar;
    public bool IsEnable { get; set; }
	// Use this for initialization
	void Start () {
        IsEnable = true;
		Instantiate (pivotSonar, transform.position, Quaternion.identity);
        StartCoroutine(loopAttack());
	}

    IEnumerator loopAttack() {
        yield return new WaitForSeconds(1f);
        if (IsEnable)
        {
            Instantiate(pivotSonar, transform.position, Quaternion.identity);
            StartCoroutine(loopAttack());
        }
        }

 /*   public void reCreatePivot() {
  
     Instantiate(pivotSonar, transform.GetChild(0).position, Quaternion.identity);
     StartCoroutine(loopAttack());
   }*/
}
