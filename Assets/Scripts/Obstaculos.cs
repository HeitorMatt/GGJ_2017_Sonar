﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Obstaculos : MonoBehaviour {

    public Interface _interfacevida;
    private bool isAllowedToTrigger = false;

    void Awake() {
        Color newColor = new Color(1, 1, 1, 0);
        GetComponent<SpriteRenderer>().color = newColor;

    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (isAllowedToTrigger == false)
            {
                StartCoroutine(FadeTo(2.5f, 1.5f));
                coll.gameObject.GetComponent<Collider2D>().isTrigger = false;
            }
            
            _interfacevida.mudancaVida();
        }

        if (coll.gameObject.tag == "Sonar") {
            
            if (isAllowedToTrigger == false)
            {
                StartCoroutine(FadeTo(2.5f, 1.5f));
                coll.gameObject.GetComponent<Collider2D>().isTrigger = true;
                coll.gameObject.SetActive(false);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player") {
            StartCoroutine(FadeTo(2.5f, 1.5f));
            _interfacevida.mudancaVida();
        }
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = GetComponent<SpriteRenderer>().color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime) // contador para o tempo de execucao
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            GetComponent<SpriteRenderer>().color = newColor;
            yield return null;
        }

        if (GetComponent<SpriteRenderer>().color.a == 0f) {
            StopCoroutine("FadeTo");
        }
        else
        {
            StartCoroutine(obstaculoOff());
        }
    }

    IEnumerator obstaculoOff() {
        yield return new WaitForSeconds(5);
        StartCoroutine(FadeTo(0.0f, 1.0f));
    }
}
