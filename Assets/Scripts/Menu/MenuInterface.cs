﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInterface : MonoBehaviour {

    public Transform menuIngame;
	ingameInterface _ingameInterface;

	void Awake()
	{
		_ingameInterface = FindObjectOfType<ingameInterface> ();
	}


    public void iniciarJogo() {
        SceneManager.LoadScene(2);
		Time.timeScale = 1f;

    }

    public void iniciarNivel2()
    {
        SceneManager.LoadScene(3);
		_ingameInterface.menuActive = false;
		Time.timeScale = 1f;
    }

    public void iniciarCreditos() {
        SceneManager.LoadScene(1);
		Time.timeScale = 1f;
    }

    public void voltarMenu() {
        SceneManager.LoadScene(0);
		Time.timeScale = 1f; 
    }

}

