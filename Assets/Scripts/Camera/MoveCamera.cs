﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    private float movementSpeed = 10f;
    private bool permissionMove = false;
    public Camera mainCameraGame;

    private string condition = "direita";

    // Update is called once per frame

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            permissionMove = true;
        }
    }

    void Update () {
        
            // mover a camera para o campo da direita
            if (permissionMove && condition == "direita")
            {
                mainCameraGame.transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
            }
            if (mainCameraGame.transform.position.x >= 18f && condition == "direita")
            {
                permissionMove = false;
                mainCameraGame.transform.Translate(0f, 0f, 0f);
                condition = "esquerda";
            }

            // mover a camera para o campo da direita
            if (permissionMove && condition == "esquerda")
            {
                mainCameraGame.transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
            }
            if (mainCameraGame.transform.position.x <= 0f && condition == "esquerda")
            {
                permissionMove = false;
                mainCameraGame.transform.Translate(0f, 0f, 0f);
                condition = "direita";
            }
        
        //-----



    }
}
