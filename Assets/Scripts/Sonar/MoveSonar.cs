﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSonar : MonoBehaviour {

	List<Transform> sonarList = new List<Transform> ();
	int sonarCount;
	float sonarDegreeZ, sonarDivision;
	private float movementSpeedSonar = 1.0f;

	void Start () {
		sonarDegreeZ = 0;
		//Pegar todos os sonares e ativa-los
		foreach(Transform child in gameObject.GetComponentInChildren<Transform> ()){
				sonarList.Add (child);
			for (int i = 0; i < sonarList.Count; i++) {
				//	print (i);
				sonarList [i].gameObject.SetActive (true);
			}
		}
		sonarCount = sonarList.Count;
	//	print (sonarCount);
		sonarDivision = 360/sonarCount;
//		print (sonarDivision);
//		print (sonarCount + "Countin");
		for (int i = 1; i < sonarList.Count; i++) {
			//print ("ALOALO");
			sonarList [i].gameObject.transform.Rotate(0,0,sonarDegreeZ + sonarDivision);
			sonarDegreeZ = sonarDegreeZ + sonarDivision;
			//print (sonarDegreeZ);

		}

	}
	

	void Update () {
		
		for (int i = 0; i < sonarList.Count; i++) {
			sonarList[i].transform.Translate(Vector3.up * movementSpeedSonar	 * Time.deltaTime);
		}
	}
}
