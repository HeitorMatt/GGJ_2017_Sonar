﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisaoConeSonar : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag != "MotionSensorEnemy")
        {
            Destroy(this.gameObject);
            Destroy(this.transform.parent.gameObject);
            //_enemyAlarm.reCreatePivot();
            if (other.gameObject.tag == "Player") {
                Time.timeScale = 0f;
            }
        }
    }

   
}
