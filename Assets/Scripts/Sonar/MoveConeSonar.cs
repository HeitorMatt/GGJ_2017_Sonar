﻿      using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveConeSonar : MonoBehaviour {
	List<Transform> sonarList = new List<Transform> ();
	int sonarCount;
	//xfloat sonarDegreeZ, sonarDivision;
	private float movementSpeedSonar = 1.0f;
    public Transform prefabConeSonar;

    void Start () {
		//sonarDegreeZ = 243f;
		foreach (Transform child in gameObject.GetComponentInChildren<Transform> ()) {
			sonarList.Add (child);
			for (int i = 0; i < sonarList.Count; i++) {
				sonarList [i].gameObject.SetActive (true);
			}
		}
        /*sonarCount = sonarList.Count;
        sonarDivision = 45/sonarCount;
        print (sonarDivision + "Division \n");
        for (int i = 0; i < sonarList.Count; i++) {
            //print ("ALOALO");
            sonarList [i].gameObject.transform.Rotate(0,0,sonarDegreeZ + sonarDivision);
            sonarDegreeZ = sonarDegreeZ + sonarDivision;
            //print (sonarDegreeZ);

        }
    */
    }




    void Update()
    {
        //if (continuous) { 
        for (int i = 0; i < sonarList.Count; i++)
        {
            sonarList[i].transform.Translate(Vector3.down * movementSpeedSonar * Time.deltaTime);
        }
        // }
    }
}
